'use strict'

const port = process.env.PORT || 3600;//cambiar puerto

const https = require ('https');
const express = require ('express'); //utiliza ya mongodb
const helmet = require("helmet");
const logger = require ('morgan');
const mongojs = require ('mongojs');
const Password = require ('./services/pass.service');
const token = require ('./services/token.service');
//const URL_DB="localhost:27017/SD";
//const URL_DB="mongodb+srv://sofia:<universidad18>@cluster0.yljbb.mongodb.net/<dbname>?retryWrites=true&w=majority"
const URL_DB="mongodb+srv://sofia:universidad18@cluster0.yljbb.mongodb.net/usuarios?retryWrites=true&w=majority"

const fs = require ('fs');

const app = express ();
app.use(helmet());
const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

var db = mongojs(URL_DB); //enlazado con la db SD
var id = mongojs.ObjectID;//Función para convertir un id textual en un objeto mongojs

//Declaramos nuestros Middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended:false})); //leer
app.use(express.json());

app.param("colecciones", (req, res, next, coleccion) => {
    console.log('param /api/:colecciones/');
    console.log('colección: ',coleccion);

    req.collection = db.collection(coleccion);//Creamos un puntero que apunta a la DB y a la tabla (coleccion) indicadas
    return next();
});
function auth (req,res,next){
    if(!req.headers.authorization){
        res.status(403).json( {
            result: 'KO',
            mensaje: "No se ha enviado el token tipo Bearer en la cabecera Authorization."

        });
        return next(new Error("Falta token de autorizacion"));
    }
    
    
    console.log(req.headers.authorization);
    if(req.headers.authorization.split(" ")[1] == "MITOKEN123456789"){
        return next();
    }

   
    
    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio"
    });
    return next(new Error ("Acceso no autorizado"));
}
//Rutas y Controladores
app.get('/api/', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);
    db.getCollectionNames((err,colecciones) =>{
        if(err) return next(err);
        console.log(colecciones);
        res.json({
            result: 'OK',
            colecciones: colecciones
        });

    });


});

app.get('/api/:colecciones', (req, res, next) => { //busca todos los objetos de la coleccion
    console.log('GET /api/:colecciones');
    console.log(req.params);
    console.log(req.collection);
   

         
    req.collection.find((err,coleccion)=> {
        if(err) return next(err);
        console.log(coleccion);
        res.json({
            result: 'OK',
            colección: req.params.colecciones,
            elementos: coleccion
        });
    });
    
   
    
});
app.get('/api/:colecciones/:id', (req, res, next) => { 
    const queId=req.params.id;
    const queColeccion=req.params.colecciones;

    req.collection.findOne({_id: id(queId)}, (err, elemento) => { 
        if (err) return next(err); 
        //console.log(elemento);
        res.json({
            result:'OK',
            colección: queColeccion,
            elementos : elemento
        }); 
    }); 
});

app.post ('/api/:colecciones/registrar', (req, res, next) => {
    const nuevoElemento=req.body;
    const usuario = { email: nuevoElemento.correo, nombre: nuevoElemento.Nombre, password : nuevoElemento.password}

    req.collection.findOne({correo: (req.body.correo)}, (err, elemento) => {
        //if(err) return next(err);
        if(elemento){
            res.status(401).json({
                result: 'Usuario ya registrado'
            });
        }else{
            Password.encriptaPassword(usuario.password) //enripto la contraseña
            .then(hash => {
                usuario.password=hash;
            
                req.collection.save(nuevoElemento,(err,elementoGuardado)=>{
                    if(err) return next(err);
            
                    res.status(201).json({
                        result: 'OK',
                        hash: hash,
                        colección: req.params.colecciones,
                        elemento: elementoGuardado
                    });
                    
                });
            });

        } 
      
    });
        
    
});
app.post ('/api/:colecciones/i', (req, res, next) => {
    
    req.collection.findOne({correo: (req.body.correo)},(err,elemento)=>{
        if(elemento){
            const Token = token.crearToken(elemento)
            
            res.status(201).json({
                result: 'OK',
                token : Token
            });
        }else{
            res.status(401).json({
                result: 'Usuario no registrado'
            });

        }
    });

});




app.put('/api/:colecciones/:id', (req, res, next) => {
    const elementoId = req.params.id;
    const queColeccion= req.params.colecciones;
    req.collection.update({_id: id(elementoId)}, {$set: req.body},
    {safe: true, multi: false}, (err, result) => {
    if (err) return next(err);
    res.json({
        result: 'OK',
        colección: queColeccion,
        _id: elementoId,
        resultado : result
        });
    });
    });

app.delete('/api/:colecciones/:id', (req, res, next) => {
    let elementoId = req.params.id;
    req.collection.remove({_id: id(elementoId)}, (err, resultado) => {
    if (err) return next(err);
    res.json(resultado);
    });
    });
https.createServer(opciones,app).listen(port, ()=> {
        console.log(`API GW RESTFull CRUD ejecutándose en https://localhost:${port}/api/{colecciones}/{id}`);
});
/*app.listen(port, () => {
    console.log(`API RESTFull CRUD ejecutándose en http://localhost:${port}/api/{colecciones}/{id}`);
});*/


